// node.js v10.5.0
// puppeteer "version": "1.8.0"
// chatworkにメッセージを送るライブラリを使用『post-chatwork-message』
// 「C:」配下に「log」「list」フォルダを作成
// 「list」フォルダ配下にいいねを押したいユーザーのURLを列挙したc:\\list\\target_name_list.csvを作成
// 「log」フォルダ配下にc:\\log\\target_name_log.txtを作成

const puppeteer = require('puppeteer');
const fs = require('fs');

//chatworkAPI
const postChatworkMessage = require('post-chatwork-message');
const CHATWORK_API_KEY = 'xxxxxx';
const roomId = 'xxxxxx';

var waitTime = 90000;
var insta_id = process.argv[2];
var insta_pass = process.argv[3];
var target_name = process.argv[4];
var waitTime_iine = parseInt(process.argv[5]);

//ファイルへの追記関数
function appendFile(path, data) {
    fs.appendFile(path, data, function(err) {
        if (err) { throw err; }
    });
}

//現在時刻の取得
function Gettime() {
    var now = new Date();
    var year = now.getYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    if (year < 2000) { year += 1900; }
    if (month < 10) { month = "0" + month; }
    if (day < 10) { day = "0" + day; }
    if (hour < 10) { hour = "0" + hour; }
    if (min < 10) { min = "0" + min; }
    if (sec < 10) { sec = "0" + sec; }
}

const options = {
    viewport: { width: 320, height: 580, },
    userAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",
};

puppeteer.launch({
    headless: false, //head less
    slowMo: 200 // slow
}).then(async browser => {
    const page = await browser.newPage();
    await page.goto('https://www.instagram.com/accounts/login/');
    await page.waitFor(waitTime);
    await page.type('input[name="username"]', insta_id);
    await page.type('input[name="password"]', insta_pass);
    await page.click('.Igw0E.IwRSH.eGOV_._4EzTm.bkEs3.CovQj.jKUp7.DhRcB');
    await page.waitFor(waitTime);
    const login_url = await page.url();

    //セキュリティコードを求められたときに待機
    if (login_url.match(/challenge/)) {
        console.log('セキュリティコードの入力を要求されています');
        await postChatworkMessage(CHATWORK_API_KEY, roomId, 'アカウント名「' + target_name + '」がセキュリティコードの入力を要求されています');
        while (login_url.match(/challenge/)) {
            const login_url_check = await page.url();
            if (!login_url_check.match(/challenge/)) { break; }
            await page.waitFor(10000);
        }
    }

    //ログのループ回数を取得し、数値型
    const read_log = fs.readFileSync('c:\\log\\' + target_name + '_log.txt', 'utf8');
    const log_index = read_log.indexOf(',');
    const log_subst = read_log.substring(0, log_index);
    var loopNumber = parseInt(log_subst) || 0;

    //リストの取得・件数の取得
    const read_list = fs.readFileSync('c:\\list\\' + target_name + '_list.csv', 'utf8');
    const lines = read_list.split('\n');
    const csvData = [];
    for (var i = 0; i < lines.length; i++) {
        var cells = lines[i].split(',');
        if (cells.length != 1) {
            csvData.push(cells);
        }
    }
    const all_list_count = lines.length;

    for (i in csvData) {
        var ii = parseInt(i) + loopNumber + 1;
        var url = csvData[ii][0];
        await page.waitFor(waitTime);
        await page.goto(url);
        await page.waitFor(waitTime);

        //response_flgを0にセット
        const response_flg = await page.evaluate(() => {
            localStorage.setItem('response_flg', 0);
        });

        //response check
        await page.on('response', async response => {
            if (400 == response.status()) {
                let gettime = new Gettime
                appendFile('c:\\log\\' + target_name + '_response_log.txt', response.status() + ',' + year + '/' + month + '/' + day + ' ' + hour + ':' + min + ':' + sec + '\r\n');
            }
        });

        //TypeError: Cannot read property '0' of undefinedのエラーが出た場合（リストがない場合）
        const readline = require("readline");
        const stream = fs.createReadStream('c:\\log\\' + target_name + '_error_log.txt', "utf8");
        var reader = readline.createInterface({ input: stream });
        reader.on("line", (data_error) => {
            if (data_error == "TypeError: Cannot read property '0' of undefined") {
                console.log('リストが終了しました');
                browser.close();
            }
        });

        //投稿写真の有無を判断
        var datas = await page.evaluate((selector) => {
            const list = Array.from(document.querySelectorAll('.v1Nh3.kIKUG._bz0w > a'));
            return list.map(data => data.href);
        });

        if (datas.length === 0) {
            console.log('not found pict :' + url);
        } else {
            await page.goto(datas[0]);
            await page.waitFor(waitTime);

            //いいねボタンのクラス取得
            const get_btn_class = await page.evaluate(() => {
                const node = document.querySelectorAll("button");
                const array = [];
                for (item of node) {
                    array.push(item.getAttribute("class"));
                }
                return array[1];
            });

            //文字取得
            const get_chara_iine = await page.evaluate(() => {
                const node = document.querySelectorAll("span");
                const array = [];
                for (item of node) {
                    array.push(item.getAttribute("aria-label"));
                }
                return array[3];
            });

            //いいねを押す
            try {
                //スペースを置換
                var transform_val = get_btn_class;
                var result = transform_val.replace(' ', '.');
                while (result !== transform_val) {
                    transform_val = transform_val.replace(' ', '.');
                    result = result.replace(' ', '.');
                }
                var btn_class_new = '.' + result;

                if (get_chara_iine == 'いいね！') {
                    await page.click(btn_class_new);
                    await page.waitFor(waitTime);
                    await page.on('response', response => {
                        if (400 == response.status()) {
                            const response_false = await page.evaluate(() => {
                                localStorage.setItem('response_flg', 1);
                            });
                        };
                    });
                } else if (get_chara_iine == '「いいね！」を取り消す') {
                    await page.waitFor(waitTime);
                    throw '「いいね！」はすでに押されています';
                } else {
                    await page.waitFor(waitTime);
                    throw '「いいね！」もしくは「いいね！を取り消す」の値が取れません';
                }

                const res_false_check = await page.evaluate(() => {
                    data = localStorage.getItem('response_flg');
                    return data;
                });

                if (res_false_check == 1) {
                    await page.waitFor(14400000);//4時間待機
                    await postChatworkMessage(CHATWORK_API_KEY, roomId, 'アカウント名「' + target_name + '」 status 400 処理を4時間中断');
                    await console.log('処理を4時間中断しました');
                }

                let gettime = new Gettime
                var filename = ii + ',' + year + '/' + month + '/' + day + ' ' + hour + ':' + min + ':' + sec + '   ' + url;
                await fs.writeFileSync('c:\\log\\' + target_name + '_log.txt', filename);
                //整理しやすいよう、csvで出力
                await appendFile('c:\\log\\' + target_name + '_exec_log.csv', url + ',' + year + '/' + month + '/' + day + ',' + hour + ':' + min + ':' + sec + '\r\n');
                await console.log(url);

                //実行件数の表示
                const c_read_log = fs.readFileSync('c:\\log\\' + target_name + '_log.txt', 'utf8');
                const c_index = c_read_log.indexOf(',');
                const c_str = c_read_log.substring(0, c_index);
                var count_now = parseInt(c_str) + 1;
                var result_count = parseInt(all_list_count);
                await console.log('list ' + ' ' + target_name + ' ' + result_count + '件 / ' + count_now + '件');
                await page.waitFor(waitTime_iine);
            } catch (e) {
                let gettime = new Gettime
                appendFile('c:\\log\\' + target_name + '_error_log.txt', year + '/' + month + '/' + day + ' ' + hour + ':' + min + ':' + sec + '\r\n' + e + '\r\n');
            }
        }
    }
});